// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import NavBar from './components/NavBar'
import ImageCarousel from './components/ImageCarousel'
import Footer from './components/Footer'

Vue.config.productionTip = false
Vue.component('nav-bar', NavBar)
Vue.component('img-carousel', ImageCarousel)
Vue.component('page-footer', Footer)
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
