import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/components/pages/Index'
import Info from '@/components/pages/Info'
import Modpack from '@/components/pages/Modpack'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Index',
      component: Index
    },
    {
      path: '/info/',
      name: 'Info',
      component: Info
    },
    {
      path: '/modpack/',
      name: 'Modpack',
      component: Modpack
    }
  ]
})
